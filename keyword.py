f = open('keywords', 'r').readlines()
o = open('out.cpp', 'w')
a = []
for keyword in f:
    try:
        idx = keyword.index('\n')
        a.append(keyword[:idx])
    except:
        pass

def helper(a, x = 1):
    if len(a) == 0 or len(a[0]) == 0:
        return
    def sub_helper(str, x):
        l = list(str)
        n = len(l)
        o.write((x + 1) * '\t' + "case '" + l.pop() + "':\n")
        x+=1
        i = 1
        while (i < n):
            o.write((x + 1) * '\t' + 'switch (c)\n' + (x + 1) * '\t' + '{\n')
            o.write((x + 1) * '\t' + "case '" + l.pop() + "':\n")
            x+=1
            i+=1
        i = 1
        while (i < n):
            o.write( x * '\t' + '}\n')
            x-=1
            i+=1

    c = a[0][-1]
    o.write(x * '\t' + "case '" + c + "':\n")
    arr = []
    for i in range(len(a)):
        arr.append(a[i][:-1])
    add = [arr[0]]
    nad = []
    j = 1
    while j<len(arr) and len(arr[i]) > 0 and arr[j].startswith(c):
        add.append(arr[j])
        j+=1
    
    if len(add) >= 1 and len(add[0]) >= 1:
        o.write((x + 1) * '\t' + 'switch (c)\n' + (x + 1) * '\t' + '{\n')
    j = 1
    while j<len(arr) and len(arr[i]) > 0 and not arr[j].startswith(c):
        sub_helper(arr[j], x)
        j+=1
    helper(add, x + 1)
    helper(nad, x + 1)    
    if len(add) >= 1 and len(add[0]) >= 1 or len(arr[0]) >= 1:
        o.write((x+1)* '\t' + '}\n')
    
a.sort()
o.write('switch (c)\n{\n')
i = 0
while i < len(a) - 1:
    next = a[i + 1]
    cur = a[i]
    if len(cur) == 0:
        pass
    curidx = len(cur) - 1
    nextidx = len(next) - 1
    cur_head = cur[curidx]
    next_head = next[nextidx]
    cur_tail = cur[:curidx]
    next_tail = next[:nextidx]
    if (next_tail == cur):
        o.write(len(next) * '\t' + 'switch (c)\n' + len(next) * '\t' + '{\n')
        i+=1
        if (len(next) < len(cur)):
            o.write(len(cur) * '\t' + '}\n')
    else:
        add = [a[i][::-1]]
        j = i + 1
        while j<len(a) and len(a[i]) > 0 and a[j].startswith(a[i][0]):
            add.append(a[j][::-1])
            j+=1
        helper(add)
        i = j

o.close()